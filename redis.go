package redis

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/go-redis/redis/v7"
	"gitlab.com/rini-golang/baseapp"
	"gitlab.com/rini-golang/config"
	// "github.com/redis/go-redis/v9"
	"github.com/rs/zerolog/log"
)

type Redis struct {
	// Config *aws.Config
	RedisClient *redis.Client
}

func NewStorage(ctx context.Context, wg *sync.WaitGroup, cfg *config.Redis) (*Redis, error) {
	if cfg == nil {
		log.Debug().Err(errors.New("Redis config is empty")).Msg("")
		return nil, errors.New("Redis config is empty")
	}

	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort),
		Password: cfg.RedisPassword, // no password set
		DB:       cfg.RedisDBName,   // use default DB
	})
	r := &Redis{}
	r.RedisClient = client

	wg.Add(1)

	go func() {
		onStop := app.StartStopFunc(ctx, wg)
		defer onStop()
		<-ctx.Done()

		err := r.RedisClient.Close()
		if err != nil {
			log.Debug().Err(err).Msg("")
		}
	}()

	return r, nil
}

func (s *Redis) Set(key, value string, ttl time.Duration) error {
	err := s.RedisClient.Set(key, value, ttl).Err()
	if err != nil {
		log.Debug().Err(err).Msg("")
		return err
	}
	// log.Debug().Msgf("Set redis key: %s,\nSet redis value: %s", key, value)
	// val, err := s.Get(key)
	// if err != nil {
	// 	log.Debug().Err(err).Msg("")
	// 	return err
	// }
	// log.Debug().Msgf("Get redis key: %s,\nGet redis value: %s", key, val)
	return nil
}

func (s *Redis) Get(key string) (val string, err error) {
	val, err = s.RedisClient.Get(key).Result()
	if err != nil {
		log.Debug().Err(err).Msg("")
		return "", err
	}

	return val, nil
}

func (s *Redis) Delete(key string) (err error) {
	err = s.RedisClient.Del(key).Err()
	if err != nil {
		log.Debug().Err(err).Msg("")
		return err
	}

	return nil
}

func (s *Redis) Client() *redis.Client {
	return s.RedisClient
}

func (s *Redis) Increase(key string) *redis.IntCmd {
	vall := s.RedisClient.Incr(key)
	log.Debug().Msgf("%+v", *vall)

	return vall
}

func (s *Redis) Decrease(key string) *redis.IntCmd {
	vall := s.RedisClient.Decr(key)
	log.Debug().Msgf("%+v", *vall)

	return vall
}

func (s *Redis) ListAdd(key, subkey, value string) *redis.IntCmd {
	vall := s.RedisClient.HSet(key, subkey, value)
	log.Debug().Msgf("%+v", *vall)

	return vall
}

func (s *Redis) ListDelOne(key, subkey string) *redis.IntCmd {
	vall := s.RedisClient.HDel(key, subkey)
	log.Debug().Msgf("%+v", *vall)

	return vall
}

func (s *Redis) ListGetOne(key, subkey string) *redis.StringCmd {
	vall := s.RedisClient.HGet(key, subkey)
	log.Debug().Msgf("%+v", *vall)

	return vall
}

func (s *Redis) ListGetAll(key string) *redis.StringStringMapCmd {
	val := s.RedisClient.HGetAll(key)
	log.Debug().Msgf("%+v", *val)

	return val
}
